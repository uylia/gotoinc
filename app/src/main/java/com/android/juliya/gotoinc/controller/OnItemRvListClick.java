package com.android.juliya.gotoinc.controller;

import com.android.juliya.gotoinc.model.Place;

/**
 * Created by juliya on 04.06.2017.
 */

public interface OnItemRvListClick {
    void onItemClik(Place place);
}
