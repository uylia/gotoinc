package com.android.juliya.gotoinc.model;


import java.io.Serializable;

/**
 * Created by juliya on 04.06.2017.
 */

public class PlaceEvent implements Serializable {
    public Place place;

    public PlaceEvent(Place place) {
        this.place = place;
    }
}
