package com.android.juliya.gotoinc.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.juliya.gotoinc.view.FragmentList;

/**
 * Created by juliya on 03.06.2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentList.newInstance(position);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
