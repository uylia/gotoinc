package com.android.juliya.gotoinc.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.juliya.gotoinc.R;
import com.android.juliya.gotoinc.controller.CustomInfoWindowAdapter;
import com.android.juliya.gotoinc.controller.RvAdapter;
import com.android.juliya.gotoinc.model.Place;
import com.android.juliya.gotoinc.model.PlaceEvent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private EventBus mEventBus;


    @Bind(R.id.sliding_layout)
    SlidingUpPanelLayout slidingUpPanelLayout;
    @Bind(R.id.ll_bottom_up_panel)
    LinearLayout bottomPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        preparingMap();
        updatePanelMargin();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    private void updatePanelMargin(){
        int allHeight = getWindowManager().getDefaultDisplay().getHeight();
        int listHeight = dpToPx(4 * 80 + 70);
        int margin = allHeight - listHeight;
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) bottomPanel.getLayoutParams();
        if( lp != null)
            lp.topMargin = margin;
    }

    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void preparingMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(PlaceEvent placeEvent){
        moveMap(placeEvent.place);
    }

    private void moveMap(Place place){
        mMap.clear();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLocation(), 15f));
       Marker marker = mMap.addMarker(new MarkerOptions().position(place.getLocation()));
        marker.setTag(place);

        marker.showInfoWindow();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick({R.id.iv_user,R.id.iv_settings, R.id.iv_search_add})
    void onBtnClick(){
        Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
    }
}
