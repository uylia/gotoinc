package com.android.juliya.gotoinc.controller;

import com.android.juliya.gotoinc.model.Place;

import java.util.Comparator;

/**
 * Created by juliya on 03.06.2017.
 */

public class SortDistance implements Comparator<Place> {
    @Override
    public int compare(Place o1, Place o2) {
        return Float.compare(o1.getDistance(), o2.getDistance());
    }
}
