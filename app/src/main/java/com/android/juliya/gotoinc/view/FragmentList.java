package com.android.juliya.gotoinc.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.juliya.gotoinc.R;
import com.android.juliya.gotoinc.controller.OnItemRvListClick;
import com.android.juliya.gotoinc.controller.RvAdapter;
import com.android.juliya.gotoinc.controller.SortDistance;
import com.android.juliya.gotoinc.controller.SortPrice;
import com.android.juliya.gotoinc.model.Place;
import com.android.juliya.gotoinc.model.PlaceEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by juliya on 03.06.2017.
 */

public class FragmentList extends Fragment {

    private static final String LIST_POSITION = "list_position";

    private RvAdapter rvAdapter;
    private ArrayList<Place> list = new ArrayList<>();
    private int position;
    private EventBus eventBus;
    @Bind(R.id.list)
    RecyclerView rvList;


    public static Fragment newInstance(int position) {
        FragmentList fragment = new FragmentList();
        Bundle bundle = new Bundle();
        bundle.putInt(LIST_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(LIST_POSITION);
        eventBus = EventBus.getDefault();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        ButterKnife.bind(this, view);
        sortList();
        preparingList();
        return view;
    }

    private void sortList() {
        list = Place.getTestData();
        switch (position) {
            case 0:
                Collections.sort(list, new SortDistance());
                break;
            case 1:
                Collections.sort(list, new SortPrice());
                break;
        }
    }

    private void preparingList() {
        rvList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAdapter = new RvAdapter(list, getActivity(), new OnItemRvListClick() {
            @Override
            public void onItemClik(Place place) {
                eventBus.post(new PlaceEvent(place));
            }
        });
        rvList.setAdapter(rvAdapter);
    }
}
