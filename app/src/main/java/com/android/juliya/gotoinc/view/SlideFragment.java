package com.android.juliya.gotoinc.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.juliya.gotoinc.R;
import com.android.juliya.gotoinc.controller.ViewPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by juliya on 03.06.2017.
 */

public class SlideFragment extends Fragment {
    private ViewPagerAdapter pagerAdapter;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.slide_fragment, container,false);
        ButterKnife.bind(this,view);
        initTabs();
        initPager();
        return view;
    }
     private void initTabs(){
         String[] names = {"По расстоянию","По стоимости"};
         for (int i = 0; i < 2; i++) {
             tabLayout.addTab(tabLayout.newTab().setTag(i));
             TextView tabView = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_layout, null);
             tabView.setText(names[i]);
             tabLayout.getTabAt(i).setCustomView(tabView);
         }
     }

    private void initPager() {
        pagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tag = (int) tab.getTag();
                pager.setCurrentItem(tag, true);
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                if (!tabLayout.getTabAt(position).isSelected())
                    tabLayout.getTabAt(position).select();
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setCurrentItem(0);
    }

}
