package com.android.juliya.gotoinc.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.juliya.gotoinc.R;
import com.android.juliya.gotoinc.model.Place;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by juliya on 04.06.2017.
 */

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    Context context;

    public CustomInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        Place place = (Place) marker.getTag();
        if(place == null) return null;
        View v = LayoutInflater.from(context).inflate(R.layout.map_lable, null);
        ((ImageView) v.findViewById(R.id.iv_map_logo)).setImageResource(place.getLogo());
        ((TextView)  v.findViewById(R.id.tv_map_title)).setText(place.getAddress());
        ((TextView) v.findViewById(R.id.tv_map_price)).setText(String.valueOf(place.getPrice()));
        return v;
    }
}
