package com.android.juliya.gotoinc.model;

import android.location.Location;

import com.android.juliya.gotoinc.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by juliya on 03.06.2017.
 */

public class Place {
    private String title;
    private String time;
    private float price;
    private int logo;
    private String address;
    private float distance;
    private LatLng location;

    public Place(String title, String time, float price, int logo, String address, float distance, LatLng location) {
        this.title = title;
        this.time = time;
        this.price = price;
        this.logo = logo;
        this.address = address;
        this.distance = distance;
        this.location = location;
    }

    public static ArrayList<Place> getTestData(){
        ArrayList<Place>list = new ArrayList<>();
        list.add(new Place("Автозаправка Shell", "час назад", 35.5f, R.drawable.shell_logo, "ул. Садовническая, 57", 0.3f, new LatLng(55.740770, 37.640539)));
        list.add(new Place("Газпром", "2 часа назад", 30.2f, R.drawable.gazprom_logo, "ул. Яузская, 9", 1.3f, new LatLng(55.747128, 37.647262)));
        list.add(new Place("Газпром", "3 часа назад", 16.8f, R.drawable.gazprom_logo, "ул. Первомайская, 33", 5.3f, new LatLng(55.793195, 37.782082)));
        list.add(new Place("Газпром", "3 часа назад", 28.5f, R.drawable.gazprom_logo, "Шоссе энтузиастов, 51", 12f, new LatLng(55.773083, 37.823849)));
        return list;
    }

    public String getTitle() {
        return title;
    }

    public String getTime() {
        return time;
    }

    public float getPrice() {
        return price;
    }

    public int getLogo() {
        return logo;
    }

    public String getAddress() {
        return address;
    }

    public float getDistance() {
        return distance;
    }

    public LatLng getLocation() {
        return location;
    }
}
