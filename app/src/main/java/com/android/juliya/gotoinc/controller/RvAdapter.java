package com.android.juliya.gotoinc.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.juliya.gotoinc.R;
import com.android.juliya.gotoinc.model.Place;
import com.android.juliya.gotoinc.model.PlaceEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by juliya on 03.06.2017.
 */

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.RvHolder> {
    private ArrayList<Place> myList = new ArrayList();
    private LayoutInflater inflater;
    private Context context;
private OnItemRvListClick onItemRvListClick;
    public RvAdapter(ArrayList<Place> myList, Context context, OnItemRvListClick onItemRvListClick) {
        this.myList = myList;
        this.context = context;
        this.onItemRvListClick= onItemRvListClick;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RvAdapter.RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_place_list, parent, false);
        return new RvHolder(view);
    }

    @Override
    public void onBindViewHolder(RvAdapter.RvHolder holder, int position) {
        final Place place = getItem(position);
        holder.tvPrice.setText(String.valueOf(place.getPrice()));
        holder.tvDistance.setText(String.valueOf(place.getDistance()));
        holder.tvTime.setText(place.getTime());
        holder.tvTitle.setText(place.getTitle());
        holder.tvAddress.setText(place.getAddress());
        holder.ivLogo.setImageResource(place.getLogo());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemRvListClick.onItemClik(place);
            }
        });
    }


    @Override
    public int getItemCount() {
        return myList.size();
    }

    private Place getItem(int position){
        return myList.get(position);
    }

    class RvHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_distance)
        TextView tvDistance;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_address)
        TextView tvAddress;
        @Bind(R.id.tv_price)
        TextView tvPrice;
        @Bind(R.id.tv_time)
        TextView tvTime;
        @Bind(R.id.iv_logo)
        ImageView ivLogo;

        public RvHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
